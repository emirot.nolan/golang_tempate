package main

import (
	"os"
	template "text/template"

	"github.com/Masterminds/sprig"
)

var temp = `
start
{{ .Title | repeat 2 | indent 2}}
{{ .Text }}
{{ first .List }}
end
`

type Values struct {
	Title string
	Text  string
	List  []int
}

func main() {
	t, err := template.New("todos").Funcs(sprig.GenericFuncMap()).Parse(temp)
	if err != nil {
		panic(err)
	}
	s := make([]int, 0)
	s = append(s, 1)
	v := Values{Title: "The title", Text: "Txt", List: s}
	err = t.Execute(os.Stdout, v)
	if err != nil {
		panic(err)
	}
}
